How to use it?

In the first console source the settings from your Vivado 2018.3 intallation:

    . /path/to/vivado/2018.3/settings64.sh 

In that console got to the "hardware" directory and run:
    
    vivado -mode batch -source AFCZ_basic.tcl
    
it will create the Vivado project: AFCZ\_basic/AFCZ\_basic.xpr you should open it in Vivado and build the bitrstream. 
Afterwards, you should export the HW (including the bitstream) locally to the project.
It will be stored in the AFCZ\_basic/AFCZ\_basic.sdk directory

In another console you should source the Petalinux settings:

    . /path/to/Petalinux2018.3/settings.sh 

In that console you should go to the "software" directory and run the "buduj.sh" script.
You should exit the initial configuration menu, that will be displayed by the script.
Finally the script should perform the complete building of the Petalinux image.

