#!/bin/bash
set -e
mkdir -p project-spec/hw-description
petalinux-config --get-hw-description ../hardware/AFCZ_basic/AFCZ_basic.sdk
petalinux-build -x mrproper ; petalinux-build
petalinux-package --boot --fsbl images/linux/zynqmp_fsbl.elf --pmufw --fpga images/linux/system.bit --u-boot --add images/linux/image.ub --file-attribute offset=0x1e00000 --force --boot-device flash

