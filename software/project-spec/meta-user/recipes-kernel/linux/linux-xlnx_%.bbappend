SRC_URI += "file://0001-net-macb-Add-MDIO-driver-for-accessing-multiple-PHY-.patch \
            file://0002-Added-delay-control-for-Marvell-88E6320.patch \
            file://user_2018-10-16-15-53-00.cfg \
            file://user_2018-10-16-23-40-00.cfg \
            file://user_2018-10-23-23-17-00.cfg \
            file://user_2018-10-24-09-35-00.cfg \
            file://user_2018-10-24-19-43-00.cfg \
            file://user_2018-11-10-00-05-00.cfg \
            file://user_2018-11-13-16-37-00.cfg \
            file://user_2018-11-13-17-13-00.cfg \
            "

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

